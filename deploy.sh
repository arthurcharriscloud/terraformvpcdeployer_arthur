#!/bin/bash
CURRENTWORKNAME=`basename $PWD`
echo "What is the name of this VPC?"
read VPCNAME
echo "What environment is this for?"
read ENVIRONMENTNAME
echo "What region is this for?"
read REGION
mkdir -p builds/$VPCNAME/$ENVIRONMENTNAME
cp *.tf builds/$VPCNAME/$ENVIRONMENTNAME
cd builds/$VPCNAME/$ENVIRONMENTNAME
sed -i '' "s,WORKNAME,$CURRENTWORKNAME,g" *.tf
sed -i '' "s,DEFAULTVPC,$VPCNAME,g" *.tf
sed -i '' "s,DEFAULTENV,$ENVIRONMENTNAME,g" *.tf
sed -i '' "s,DEFAULTREGION,$REGION,g" *.tf
terraform init
terraform plan
terraform apply
cd ../../../
