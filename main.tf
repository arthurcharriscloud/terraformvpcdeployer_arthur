provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "arthur-charris-tfstate"
    key    = "DEFAULTVPC/DEFAULTENV/terraform_dev.tfstate"
    region = "us-west-2"
  }
}

